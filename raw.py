# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# ## 1. Description of the paper’s main question and data. (Keep your answer brief no more than 1 page) In your description, make sure to answer the following questions:
# 1. What question does the author attempt to answer?
# 2. How is eligibility for veterans’ benefits determined?
# 3. Who is included in their study and where do the data come from?
# %% [markdown]
# ## 2. Replicate all of Table 1 from Angrist (1993). Things to keep in mind:
# - The best grades will go to the closest replications. The most important element is the accuracy of all numbers, but style is also important.
# - You should be able to match the sample size and most numbers exactly, but you will notice that once you do that correctly, there will be a couple of numbers that are a little off. This is OK, but you should note which ones are different.
# - Make sure that you carefully read all notes in the table as well as the part of the text that describes the data. This is where you’ll find an explanation of the sample selection as well as a description of which numbers appear in the table.
# - You must use Stata (or a different statistical software) to calculate all of the numbers in the table, but you may use any software you’d like to actually put the numbers together into a table.

# %%
import pandas as pd


# %%
df = pd.io.stata.read_stata("Angrist1993Replication.dta")
df.head()

# %% [markdown]
# ## 3. Replicate Figures 1 and 2 given on page 3 of this assignment. Things to keep in mind:
# - All figures were created using the sample from Table 1. You probably want to make sure your numbers from Table 1 are correct before making the figures.
# - Again, the best grades will go to the closest replications. The most important element is that the figure displays the correct data, but style also matters (i.e. having a white border instead of Stata’s default, the same legend labels, etc.). You’ll find Stata’s help files and internet searches especially helpful here!
# - You must use Stata (or a different statistical software) to create the figures, but may paste them into word or another text editor when compiling the final output file.

